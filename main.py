# C4.5 algorithm to practice machine learning in python. This project was not intended to work for a variety of
# datasets but it should be capable as long as its fed only numeric data and its classification at the end. This
# project was not tested with different numbers of columns but assuming I made it general enough it should be fine.

import random
import math
import numpy

# settings
file_name = "iris2.data"
optionA = "Iris-virginica\n"
optionB = "Iris-versicolor\n"
num_of_features = 4 # number of features within the dataset. This could be automated but since this is a "quick"
# practice project I'm not going to bother
minimum_leaf_set = 3 #this settings is the minimum number of values that can be within a leaf, settings this low
# could cause danger of overfitting
proportion_for_training = 0.7

class node:
    def __init__(self, left, right, split, split_attribute, is_split, is_pure, decision, optionA_count, optionB_count):
        self.left = left
        self.right = right
        self.split = split
        self.split_attribute = split_attribute
        self.is_split = is_split
        self.is_pure = is_pure
        self.decision = decision
        self.optionA_count = optionA_count
        self.optionB_count = optionB_count

def countOptions(data, num_of_features):
    optionA_count = 0
    optionB_count = 0
    for line in data:
        if line[num_of_features] == optionA:
            optionA_count += 1
        else:
            optionB_count += 1
    return optionA_count, optionB_count

def findGains(training_data, num_of_features):
    feature_list = list(range(0, num_of_features, 1)) #this is used a couple times later in the function

    # calculate global_entropy
    total_outcomeA_in_set = 0 #counts how many times outcome A comes up in the entire set
    total = len(training_data)
    for line in training_data:
        if line[num_of_features] == optionA:
            total_outcomeA_in_set += 1

    global_entropy = -(total_outcomeA_in_set / total) * math.log(total_outcomeA_in_set / total, 2) - (
            (total - total_outcomeA_in_set) / total) * math.log((total - total_outcomeA_in_set) / total, 2)

    # these 4 lines are used for finding possible split values, and they are not very good at it. They work for this
    # dataset reasonably well but if the attributes had different scaling this would not work.
    #
    # the alternative would be to exhaustively search through the dataset attribute values but this would be
    # different scaling for each attribute which means each attribute couldn't be done in parallel as is currently
    # done and would cause slow-down for large datasets. BUT doing it this way could make multi-threading easier (
    # Maybe?)
    #
    # another option could be to enforce normalization of input data and these 4 lines would be fine as they are.
    #
    # Finally a sampling algorithm could be used to determine optimum values without needing normalized data.
    #
    # Now you might be wondering, "Megan, if there are  problems with these lines why don't you fix them?" and the
    # answer to that is because this was supposed to be quick practice project and I need to do some other projects
    # as well before the semester break is over.
    min_val = 0.1
    max_val = 7
    samples = 100
    possible_splits = numpy.linspace(min_val, max_val, samples)

    # finds gains and respective split vals. questionable accuracy
    best_gains = [0] * num_of_features
    best_split_val = [0] * num_of_features
    for split_val in possible_splits:
        times_less_than_split_val = [0] * num_of_features #total number of times a value is below the split value
        # regardless of outcome
        times_less_than_split_val_is_A = [0] * num_of_features #total number of times a value is below the split
        # value AND its outcome is A
        times_greater_than_split_val = [0] * num_of_features #same as above but greater than split value.
        times_greater_than_split_val_is_A = [0] * num_of_features

        #counting loop for 4 variables above
        for line in training_data:
            for feature_index in feature_list:
                if line[feature_index] <= split_val:
                    times_less_than_split_val[feature_index] += 1
                    if line[num_of_features] == optionA:
                        times_less_than_split_val_is_A[feature_index] += 1
                else:
                    times_greater_than_split_val[feature_index] += 1
                    if line[num_of_features] == optionA:
                        times_greater_than_split_val_is_A[feature_index] += 1

        for i in feature_list:
            if times_less_than_split_val[i] == 0 or times_greater_than_split_val[i] == 0:  # division by zero check
                continue

            # past this point within this for loop i am not really sure how the math works. It was made from the
            # examples from this source and then made into a general form:
            # https://sefiks.com/2018/05/13/a-step-by-step-c4-5-decision-tree-example/
            # what i do know about the math is it arrives at a gain for each feature of the dataset and later the
            # feature to use for a split is determined from its respective gain
            p_no_less = (times_less_than_split_val[i] - times_less_than_split_val_is_A[i]) / times_less_than_split_val[
                i]
            p_yes_less = times_less_than_split_val_is_A[i] / times_less_than_split_val[i]
            p_no_greater = (times_greater_than_split_val[i] - times_greater_than_split_val_is_A[i]) / \
                           times_greater_than_split_val[i]
            p_yes_greater = times_greater_than_split_val_is_A[i] / times_greater_than_split_val[i]

            entropy_lessthan = 0
            entropy_greaterthan = 0

            # these 2 complicated if chains is to prevent having log(0)
            if p_no_less > 0 and p_yes_less > 0:  # this is ideal case
                entropy_lessthan = -p_no_less * math.log(p_no_less, 2) - p_yes_less * math.log(p_yes_less, 2)
            if p_no_less > 0 and p_yes_less <= 0:
                entropy_lessthan = -p_no_less * math.log(p_no_less, 2)
            if p_no_less <= 0 and p_yes_less > 0:
                entropy_lessthan = - p_yes_less * math.log(p_yes_less, 2)

            if p_no_greater > 0 and p_yes_greater > 0:  # this is ideal case
                entropy_greaterthan = -p_no_greater * math.log(p_no_greater, 2) - p_yes_greater * math.log(
                    p_yes_greater, 2)
            if p_no_greater > 0 and p_yes_greater <= 0:
                entropy_greaterthan = -p_no_greater * math.log(p_no_greater, 2)
            if p_no_greater <= 0 and p_yes_greater > 0:
                entropy_greaterthan = - p_yes_greater * math.log(p_yes_greater, 2)

            #this part is the whole point of this function
            gain = global_entropy - times_less_than_split_val[i] / total * entropy_lessthan - \
                   times_greater_than_split_val[i] / total * entropy_greaterthan

            if gain > best_gains[i]:
                best_gains[i] = gain
                best_split_val[i] = split_val

    return best_gains, best_split_val

def makeTree(training_data, num_of_features,optionA,optionB):

    #these 3 if statements are the stopping points of the tree. if the dataset has gotten too small it will stop or
    # if the leaf is pure.
    A,B = countOptions(training_data,num_of_features)
    if A == 0:
        return node(0,0,0,0,False,True,optionB,A,B) #no left or right nodes, no split val, no attribute, not split,
        # is pure
    if B == 0:
        return node(0,0,0,0,False,True,optionA,A,B)

    if len(training_data) <= minimum_leaf_set:
        if A > B:
            return node(0,0,0,0,False,False,optionA,A,B) #no left or right nodes, no split val, no attribute, not split,
        # NOT pure
        else:
            return node(0,0,0,0,False,False,optionB,A,B)

    gains, splits = findGains(training_data,num_of_features) #math computation function

    # finds which column is best to use for split
    feature_list = list(range(0, num_of_features, 1))
    best_gain_pos = 0
    best_gain = 0
    for i in feature_list:
        if gains[i] > best_gain:
            best_gain = gains[i]
            best_gain_pos = i

    # needs to be sorted for split to work. this should be called every iteration because the column to sort by may
    # change
    training_data = sorted(training_data, key=lambda x:x[best_gain_pos])

    #finds where in the list the split point is based on the split value
    split_pos = 0
    pos = 0
    for line in training_data:
        if line[best_gain_pos] > splits[best_gain_pos]:
            split_pos = pos
            break
        pos += 1

    #data split
    training_data_left = training_data[:split_pos]
    training_data_right = training_data[split_pos:]

    #find left and right nodes recursively then adds them to the main node to be returned
    left_node = makeTree(training_data_left,num_of_features,optionA,optionB)
    right_node = makeTree(training_data_right,num_of_features,optionA,optionB)
    return node(left_node, right_node, splits[best_gain_pos], best_gain_pos, True, False, 0, A, B)

def printTree(indent, node):
    indent_list = list(range(0, indent, 1))
    for i in indent_list:
        print("    ",end='') #prevents python adding a new line every time

    if node.is_pure == True:
        print("A:",node.optionA_count,"B:",node.optionB_count,"Decision:",node.decision)
    else:
        print("Split:",node.split,"Split attribute:", node.split_attribute,"A:",node.optionA_count,"B:",node.optionB_count)

    if node.is_split == False:
        return

    printTree(indent+1,node.left)
    printTree(indent+1,node.right)

def useTree(tree,row):
    if tree.is_split is False:
        return tree.decision

    if row[tree.split_attribute] <= tree.split:
        return useTree(tree.left,row)
    else:
        return useTree(tree.right,row)

# importing file data
file_object = open(file_name, "r")  # r is for read only
all_lines_string = file_object.readlines()
file_object.close()

# returns a 2D list containing values and correct classification from data file
data_list = list(list())
for line in all_lines_string:
    elements = line.split(",")
    if len(elements) == num_of_features + 1:
        new_object = list()
        for elem in elements:
            if "\n" in elem:
                new_object.append(elem)
            else:
                new_object.append(float(elem))
        data_list.append(new_object)

# shuffles data list then splits into 70:30 training and test data
random.shuffle(data_list)
index70 = int(
    len(data_list) * proportion_for_training)  # returns an index that represents 70% of the list.
training_data = data_list[:index70]
test_data = data_list[index70:]

#all computations to make the tree are called from here
tree = makeTree(training_data, num_of_features,optionA,optionB)

#prints the tree to console with indentation
printTree(0,tree)

#validates the model
correct = 0
incorrect = 0
for line in test_data:
    estimate = useTree(tree,line)
    if estimate == line[num_of_features]:
        correct += 1
    else:
        incorrect += 1
print("Correct Estimations:", correct,"Incorrect Estimations:",incorrect)